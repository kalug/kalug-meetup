Hello, we ara KaLUG (Kaohsiung Linux User Group).

We love Open Source and love to share this experience with others. 

# Where to find us.

Web site:
https://kalug.linux.org.tw

Telegram chat group:
https://t.me/joinchat/AAAAAEF9EuWf_Ze0Yg9geA

KKTIX: (Meetups)
http://kalug.kktix.cc/

# Our meetups !!

## Every Thursdays (Tainan)

Shawn and Silice live in Tainan, we host a weekly meetup in Tainan.

PM: 6:30 ~ 8:30
Location: Tainan

40 mins: weekly topic
others: free discussion

## Monthly Events (Kaohsiung)

When: one Saturday afternoon PM: 2:00 ~ 5:00
Location: Kaohsiung